#! ruby

require 'getoptlong'

###########################################################
def read_input(input, index, final)
  File.open(input,'r').each_line do |line|
    line.rstrip!
    final[index].push(line)
  end
end


###########################################################
inputs = Array.new
final = Hash.new{|h,k|h[k]=[]}

opts = GetoptLong.new(
  ['-i', GetoptLong::REQUIRED_ARGUMENT],
)

opts.each do |opt,value|
  case opt
    when '-i'
      inputs.push value
  end
end


###########################################################
inputs.each_with_index do |input, index|
  read_input(input,index,final)
end

max = final.values.map{|x|x.size}.max

0.upto(max-1) do |i|
  tmp_list = Array.new
  final.each_pair do |k,v|
    if final[k].size >= i+1
      tmp_list.push v[i]
    else
      tmp_list.push ''
    end
  end
  puts tmp_list.join("\t")
end


