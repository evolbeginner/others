#! /bin/env ruby

require 'getoptlong'

regexp_for_gene=nil
list1=Array.new
list2=Array.new
list_ele=Hash.new{|h,k|h[k]={}}
sep1, sep2 = "\t", "\t"
field1, field2 = 1, 1
is_report = true
show_item = nil
out_sep = "\n"


#######################################################################
def read_lists(lists=[], fields=[1,1], seps=["\t","\t"], regexp_for_gene=nil)
  list_ele=Hash.new{|h,k|h[k]=Hash.new()}
  raise "lists cannot be empty" if lists.empty?
  lists.each_with_index do |list,index|
    list.each do |list_file|
      fh=File.open(list_file,'r')
      fh.each_line do |line|
        line.chomp!
        ele=line.split(seps[index])[fields[index]-1]
        (ele=$1 if line=~/(#{regexp_for_gene})/) if ! regexp_for_gene.nil?
        list_ele[index][ele]=1
      end
      fh.close
    end
  end
  return list_ele
end


#######################################################################
opts = GetoptLong.new(
  ['--i1', '--list1', GetoptLong::REQUIRED_ARGUMENT],
  ['--i2', '--list2', GetoptLong::REQUIRED_ARGUMENT],
  ['--sep1', GetoptLong::REQUIRED_ARGUMENT],
  ['--sep2', GetoptLong::REQUIRED_ARGUMENT],
  ['--f1', GetoptLong::REQUIRED_ARGUMENT],
  ['--f2', GetoptLong::REQUIRED_ARGUMENT],
  ['--regexp_for_gene', GetoptLong::REQUIRED_ARGUMENT],
  ['--show', GetoptLong::REQUIRED_ARGUMENT],
  ['--no_report', GetoptLong::NO_ARGUMENT],
)

opts.each do |opt,value|
  case opt
    when '--i1', '--list1'
      list1.push value
    when '--i2', '--list2'
      list2.push value
    when '--sep1'
      sep1=value
    when '--sep2'
      sep2=value
    when '--f1'
      field1=value.to_i
    when '--f2'
      field2=value.to_i
    when '--regexp_for_gene'
      regexp_for_gene=value
    when '--show'
      show_item = value
    when '--no_report'
      is_report = false
  end
end

#######################################################################
list_ele = read_lists([list1,list2], [field1, field2], [sep1,sep2], regexp_for_gene)

eles_shared_by_0_and_1 = list_ele[0].keys & list_ele[1].keys

if is_report
  list_ele.each_with_index do |ele,index|
    a=[index, eles_shared_by_0_and_1.size, list_ele[index].keys.size].map{ |i| i.to_s }
    out=a.join("\t")
    print out, "\t", a[1].to_f/a[2].to_f, "\n"
  end
  puts
end

if show_item
  if show_item == "1"
    puts (list_ele[0].keys - eles_shared_by_0_and_1).join(out_sep)
  elsif show_item == "2"
    puts (list_ele[1].keys - eles_shared_by_0_and_1).join(out_sep)
  elsif show_item == "12" or show_item == "overlap" or show_item == "intersect"
    puts eles_shared_by_0_and_1.join(out_sep)
  elsif show_item =~ /comple/i
    puts (list_ele1).join(out_sep)
    puts
    puts (list_ele2).join(out_sep)
  end
  puts
end


